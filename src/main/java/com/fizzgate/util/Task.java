package com.fizzgate.util;

import lombok.Builder;

@Builder
public class Task implements Runnable {
    private Integer tId;
    private Runnable work;


    @Override
    public void run() {
        try {
            work.run();
        } catch (Exception exception) {
            exception.printStackTrace();
        } finally {

        }
    }
}