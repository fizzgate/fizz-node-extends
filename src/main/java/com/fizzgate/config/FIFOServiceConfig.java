package com.fizzgate.config;

import com.fizzgate.util.FIFOService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FIFOServiceConfig {
    @Bean
    public FIFOService dynaimcPluginTask() {
        FIFOService fifoService = new FIFOService();
        fifoService.startTaskHandle();
        return fifoService;
    }

}
