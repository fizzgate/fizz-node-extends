package com.fizzgate.dynamic.event.handle;

import lombok.Data;
import java.util.List;

@Data
public class NodeBizChangeConfig {
    public boolean force;
    public List<String> filenames;
}
