package com.fizzgate.dynamic.event.handle;

import com.fizzgate.plugin.FizzPluginFilter;

import java.util.HashMap;
import java.util.Map;

public class BizFilterPluginCache {
    static private Map<String, FizzPluginFilter> filterPlugins;

    public static void put(String name, FizzPluginFilter dynamicPluginFilter) {
        filterPlugins.put(name, dynamicPluginFilter);
    }


    public static void cleanup() {
        filterPlugins = new HashMap<>();
    }

    public static void init() {
        filterPlugins = new HashMap<>();
    }

    public static FizzPluginFilter get(String plugin) {
        return filterPlugins.get(plugin);
    }

    public static void remove(String name) {
        filterPlugins.remove(name);
    }
}
