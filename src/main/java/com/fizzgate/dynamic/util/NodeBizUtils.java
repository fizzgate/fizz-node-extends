package com.fizzgate.dynamic.util;

import com.alipay.sofa.ark.spi.model.Biz;

public class NodeBizUtils {
    public static boolean isArkBiz(Biz source){
        Class<?> bizClassLoader  = source.getBizClassLoader().getClass();
        System.out.println(bizClassLoader.getCanonicalName());
        return "com.alipay.sofa.ark.container.service.classloader.BizClassLoader".equals(bizClassLoader.getCanonicalName());
    }
}
