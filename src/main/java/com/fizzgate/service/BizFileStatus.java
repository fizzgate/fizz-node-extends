package com.fizzgate.service;

import lombok.Getter;

@Getter
public enum BizFileStatus {
    DOWNLOADING(200, "下载中"),
    DOWNLOADED(400, "下载完成");
    final int code;
    final String message;

    private BizFileStatus(final int code, final String message) {
        this.code = code;
        this.message = message;
    }
}
