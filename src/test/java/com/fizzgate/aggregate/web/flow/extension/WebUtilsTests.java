package com.fizzgate.aggregate.web.flow.extension;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fizzgate.util.WebUtils;

import org.junit.jupiter.api.Test;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author hongqiaowei
 */

public class WebUtilsTests {

    @Test
    void testWebClient() {
        String managerUrl = "https://demo.fizzgate.com";
        Map errorMsg = new HashMap<String, Object>();
        WebClient webClient = WebClient.create(managerUrl);
        Map<String, Object>  data = (Map<String, Object>) webClient.get()
                .uri("/api/fizz-manager/packae/page")
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<Map<String,Object>>(){})
                .doOnError(WebClientResponseException.class, err -> {
                    errorMsg.put("msg",  err.getLocalizedMessage());
                    throw new RuntimeException(err.getResponseBodyAsString());
                })
                .onErrorReturn(errorMsg)
                .block();
        System.out.print(data);
    }


}
