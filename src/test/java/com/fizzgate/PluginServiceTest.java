package com.fizzgate;

import com.alipay.sofa.ark.container.service.biz.BizManagerServiceImpl;
import com.alipay.sofa.ark.container.model.BizModel;
import com.alipay.sofa.ark.api.ArkClient;
import com.fizzgate.service.PluginService;
import com.fizzgate.util.ReflectionUtils;
import com.fizzgate.util.FIFOService;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.springframework.test.context.TestPropertySource;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static java.lang.Thread.sleep;

@TestPropertySource("/application.properties")
@SpringJUnitConfig()
public class PluginServiceTest {

    private PluginService pluginService ;
    private
    FIFOService fifoService = new FIFOService();

    @BeforeEach
    void beforeEach() throws NoSuchFieldException {
        pluginService = new PluginService();
        ReflectionUtils.set(pluginService, "bizDir", "/Users/linwaiwai/Documents/Project/fizz/fizz-dynamic-plugin/fizz-plugin/target/");
//        ReflectionUtils.set(pluginService, "bizPackgeUrl", "https://demo.fizzgate.com");

        fifoService.startTaskHandle();
    }
    @Test
    void test() throws Throwable {
        List<Map> bizs = new ArrayList<>();
        Map<String, String> biz = new HashMap<String, String>();
        biz.put("filename", "fizz-plugin-1.0.0-SNAPSHOT-ark-biz");
        biz.put("url", "file:///Users/linwaiwai/Documents/Project/fizz/fizz-dynamic-plugin/fizz-plugin/target/fizz-plugin-1.0.0-SNAPSHOT-ark-biz.jar");
        bizs.add(biz);
        BizManagerServiceImpl bizManagerService = Mockito.mock(BizManagerServiceImpl.class);
        MockedStatic<ArkClient> arkClient = Mockito.mockStatic(ArkClient.class);
        arkClient.when(()->ArkClient.getBizManagerService()).thenReturn(bizManagerService);

        MockedStatic<ArkClient> fizzArkClient = Mockito.mockStatic(ArkClient.class);

        BizModel bizModel = new BizModel();
        bizModel.setBizName("fizz-plugin");
        bizModel.setBizVersion("1.0.0-SNAPSHOT");
//        fizzArkClient.when(()->ArkClient.createBiz(Mockito.any())).thenReturn(bizModel);
        Mockito.when(bizManagerService.isActiveBiz(Mockito.anyString(),Mockito.anyString())).thenReturn(true);

        fifoService.addTask(()->{
            pluginService.downloadBizs(bizs);
            pluginService.closeBizs(bizs);
            pluginService.startBizs(bizs);
        });

        sleep(1000000);

    }
}